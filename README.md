# mvvmfw

[![CI Status](http://img.shields.io/travis/Duy Nguyen/mvvmfw.svg?style=flat)](https://travis-ci.org/Duy Nguyen/mvvmfw)
[![Version](https://img.shields.io/cocoapods/v/mvvmfw.svg?style=flat)](http://cocoapods.org/pods/mvvmfw)
[![License](https://img.shields.io/cocoapods/l/mvvmfw.svg?style=flat)](http://cocoapods.org/pods/mvvmfw)
[![Platform](https://img.shields.io/cocoapods/p/mvvmfw.svg?style=flat)](http://cocoapods.org/pods/mvvmfw)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

mvvmfw is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "mvvmfw"
```

## Author

Duy Nguyen, duy.nguyen@pops.vn

## License

mvvmfw is available under the MIT license. See the LICENSE file for more info.
