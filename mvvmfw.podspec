#
# Be sure to run `pod lib lint mvvmfw.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'mvvmfw'
  s.version          = '0.1.4'
  s.summary          = 'Swift MVVM Framework using Rx'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
There are many ways to apply MVVM concept, and this is one variant. This approach uses Rx to connect layers
                       DESC

  s.homepage         = 'https://gitlab.com/popsvn-public/swift-mvvm-framework'
  s.screenshots     = 'https://raw.githubusercontent.com/rickyngk/swift-mvvm/master/assets/MVVM.png'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Duy Nguyen' => 'duy.nguyen@pops.vn' }
  s.source           = { :git => 'https://gitlab.com/popsvn-public/swift-mvvm-framework.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'mvvmfw/Classes/**/*'
  
  # s.resource_bundles = {
  #   'mvvmfw' => ['mvvmfw/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'

  s.frameworks = 'UIKit'

  s.dependency 'RxSwift', '~> 3.0.0'
end
