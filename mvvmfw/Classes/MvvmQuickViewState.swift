//
//  MvvmQuickViewState.swift
//  Pods
//
//  Created by NGUYEN KHANH DUY on 8/11/16.
//
//

import Foundation

open class MvvmQuickViewState: NSObject, MvvmViewState {
    required override public init(){}
}

extension MvvmQuickViewState {
    func propertyNames() -> [String] {
        var names: [String] = []
        var count: UInt32 = 0
        let properties = class_copyPropertyList(classForCoder, &count)
        for i in 0 ..< Int(count) {
            let property: objc_property_t = properties![i]!
            let name: NSString = NSString(cString: property_getName(property), encoding: String.Encoding.utf8.rawValue)!
            names.append(name as String)
        }
        free(properties)
        return names
    }
    
    public func update(jsonInfo: NSDictionary?) -> Self {
        let object = type(of: self).init()
        
        for key in propertyNames() {
            let keyName = key
            if (responds(to:NSSelectorFromString(keyName))) {
                if let val = jsonInfo![key] {
                    object.setValue(val, forKey: keyName)
                } else {
                    object.setValue(value(forKey: keyName), forKey: keyName)
                }
            }
        }
        return object
    }
    public func update(key: String, object: NSObject?) -> Self {
        let obj = type(of: self).init()
        
        for key in propertyNames() {
            let keyName = key 
            if (responds(to:NSSelectorFromString(keyName))) {
                if keyName == key {
                    if (object == nil) {
                        obj.setNilValueForKey(keyName);
                    } else {
                        obj.setValue(object, forKey: keyName)
                    }
                } else {
                    obj.setValue(value(forKey: keyName), forKey: keyName)
                }
            }
        }
        return obj
    }
    
}
