//
//  MvvmCommonViewModel.swift
//  mvvm-demo
//
//  Created by NGUYEN KHANH DUY on 5/18/16.
//  Copyright © 2016 NGUYEN KHANH DUY. All rights reserved.
//

import Foundation
import RxSwift

open class MvvmCommonViewModel: MvvmViewModelProtocol {
    public var viewStateStream = PublishSubject<(MvvmViewState, MvvmViewState)>()
    var _oldState: MvvmViewState?
    var disposeBag = DisposeBag()
    
    public init() {}
    
    public var viewState:MvvmViewState?  {
        willSet(newState) {
            _oldState = self.viewState
        }
        didSet {
            if (_oldState == nil) {
                viewStateStream.onNext((self.viewState!, MvvmViewStateNull.sharedInstance))
            } else {
                viewStateStream.onNext((self.viewState!, _oldState!))
            }
        }
    }
    
    open func execute(command:Any, data:Any? = NSNull()) {}
    
    public func getViewState() -> MvvmViewState {
        return viewState!
    }
    
    public func setViewState(viewstate vs: MvvmViewState) {
        self.viewState = vs
    }
}




