//
//  Mvvm+UIView.swift
//  Pods
//
//  Created by NGUYEN KHANH DUY on 6/22/16.
//
//

import Foundation
import UIKit
import RxSwift

extension MvvmCommonViewDelegate where Self: MvvmUIView {
    public func injectViewModel(viewModel: MvvmCommonViewModel) -> MvvmCommonViewModel {
        viewModel.viewStateStream.subscribe(onNext: { state in
            let newViewState = state.0
            
            if state.1 is MvvmViewStateNull {
                self.onMvvmViewStateInit(viewState: newViewState)
                self.onMvvmViewStateUpdated(viewState: newViewState)
            } else {
                self.onMvvmViewStateChanged(newViewState: newViewState, oldViewState: state.1 as MvvmViewState)
                self.onMvvmViewStateUpdated(viewState: newViewState)
            }
            
        }).addDisposableTo(viewModel.disposeBag)
        self.viewModel = viewModel
        return viewModel
    }
    
    public func execute(command:Any, data:Any? = NSNull()) -> MvvmCommonViewModel {
        viewModel!.execute(command: command, data: data)
        return viewModel!
    }
}

open class MvvmUIView: UIViewController {
    public var viewModel:MvvmCommonViewModel? = nil
}
